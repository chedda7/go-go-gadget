package main

import (
    "strings"
    "fmt"
    "os"
)

func get_system() (ret []string) {
  env_vars := os.Environ()
  return env_vars
}

func get_mesos_task(env_vars []string) {
  for i := 0; i < len(env_vars); i++ {
    s := strings.Split(env_vars[i], "=")
    // if s[0] == "MESOS_TASK_ID" {
    //   fmt.Println(s[0])
    // }
    fmt.Println(fmt.Sprintf("%s = %s", s[0], s[1]))
  }
}

func main() {
  env_vars := get_system()
  get_mesos_task(env_vars)
}
